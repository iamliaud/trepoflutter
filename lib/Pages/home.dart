import 'dart:math' as math;

import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _AnimatedPageState createState() => _AnimatedPageState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      )
    );
  }
}

var assetsImage = new AssetImage('assets/CM.jpg'); //<- Creates an object that fetches an image.

class _AnimatedPageState extends State<Home> with SingleTickerProviderStateMixin  {
  double _h = 200;

  /*void _switch() {
    setState(() {
      _h = 200 + Random().nextDouble() * 400;
    });
  }*/

  AnimationController animationController;
  Animation _armController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 7),
    )..repeat();

    _armController = animationController
    .drive(CurveTween(curve: Curves.decelerate));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: Container(
            width: 330.0,
            height: 220,
            decoration: BoxDecoration(
              color: Colors.brown[700],
              border: Border.all(
                  width: 2.0
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(10.0)
              ),
            ),
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: Container(
                  )
                ),
                Positioned.fill(
                  child:  AnimatedBuilder(
                    animation: animationController,
                    child: Stack(
                      alignment: Alignment(0.00,0.00),
                      children: <Widget>[
                        ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              "assets/vinyl_placeholder.png",
                              width: 180.0,
                              height: 180.0,
                            )
                        ),
                        ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              "assets/CM.jpg",
                              width: 72.0,
                              height: 72.0,
                            )
                        ),
                      ],
                    ),
                    builder: (BuildContext context, Widget child) {
                      return Transform.rotate(
                        angle: animationController.value * 2.0 * math.pi,
                        child: child,
                      );
                    },
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                    child: AnimatedBuilder(
                      animation: animationController,
                      child: Container(
                        color: Colors.white54,
                        child: Image.asset(
                          "assets/bras_platine.png",
                          width: 80.0,
                          height: 200.0,
                        ),
                      ),
                      builder: (BuildContext context, Widget child) {
                        return Transform.rotate(
                          angle: animationController.value * 0.8 ,
                          alignment: Alignment.center,
                          origin: Offset(3.0, -47.0),
                          child: child,
                        );
                      },
                    ),
                ),
                Positioned(
                  top: 49,
                  right: 32,
                  child: Container(
                    width: 10,
                    height: 10,
                    color: Colors.red,
                  ),
                ),
              ],
            ),
        ),
      ),
    );
  }
}

